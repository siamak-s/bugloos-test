<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::namespace('API\V1')->prefix('v1')->group(function () {
    Route::apiResource('list-configurations', 'ListConfigurationController')->only(['store']);
    Route::apiResource('users', 'UserController')->only(['index']);

    Route::prefix('users')->group(function () {
        Route::get('/index-2', 'UserController@index2');
        Route::get('/index-3', 'UserController@index3');
        Route::get('/index-4', 'UserController@index4');
        Route::get('/index-5', 'UserController@index5');

    });
});
