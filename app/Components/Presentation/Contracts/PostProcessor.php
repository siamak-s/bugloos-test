<?php


namespace App\Components\Presentation\Contracts;


abstract class PostProcessor extends Processor
{
    public abstract function postProcess(array $values) : array;
}
