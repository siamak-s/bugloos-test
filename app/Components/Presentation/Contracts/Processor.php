<?php


namespace App\Components\Presentation\Contracts;


use App\Components\Presentation\Traits\TypesHelperTrait;
use App\Components\Presentation\Utils\FieldCollection;
use Illuminate\Support\Str;

abstract class Processor implements BaseUnit
{
    use TypesHelperTrait;

    public function getKey(): string
    {
        return Str::kebab(Str::afterLast(get_class($this), "\\"));
    }

    public function getMetaData(): array
    {
        return [];
    }

    public static function create(string $name, array $configuration = []) {
        /** @var Processor $processor */
        $processor = null;

        if (PreProcessor::isValidByName($name)) {
            $processor = PreProcessor::getInstanceOf($name);
            $processor->load($configuration);
        } else if (PostProcessor::isValidByName($name)) {
            $processor = PostProcessor::getInstanceOf($name);
            $processor->load($configuration);
        }

        return $processor;
    }

    public function load(array $configuration) {
        if (empty($configuration))
            return;

        if (property_exists(get_class($this), 'collection')) {
            $this->collection = FieldCollection::fromArray($configuration);
        } else {
            foreach ($configuration as $key=>$value) {
                if (property_exists(get_class($this), $key)) {
                    $this->$key = $value;
                }
            }
        }
    }
}
