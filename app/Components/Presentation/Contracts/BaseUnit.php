<?php


namespace App\Components\Presentation\Contracts;


interface BaseUnit
{
    /**
     * @return string
     */
    public function getKey() : string;

    /**
     * @return array
     */
    public function getMetaData() : array;
}
