<?php


namespace App\Components\Presentation\Contracts;


use App\Components\Presentation\Exceptions\SourceCouldNotBeEmptyException;
use App\Components\Presentation\Presentation;
use App\Components\Presentation\Traits\ArrayHelperTrait;
use App\Components\Presentation\Utils\MetaData;
use App\Models\Model;
use Illuminate\Support\Collection;

abstract class Presenter
{
    use ArrayHelperTrait;

    /**
     * @var BaseUnit[]
     */
    private array $metaDateItems = [];

    /**
     * @var PreProcessor[]
     */
    private array $preProcessors = [];

    /**
     * @var PostProcessor[]
     */
    private array $postProcessors = [];

    /**
     * @param $source
     * @return Collection
     */
    protected abstract function execute($source) : Collection;

    /**
     * @return mixed
     */
    protected abstract function source();

    public function __construct(array $configuration = [])
    {
        $this->loadFromConfiguration($configuration);
    }

    /**
     * @param PreProcessor $preProcessor
     * @return Presenter
     */
    public function addPreProcessor(PreProcessor $preProcessor) {
        $this->addOrReplaceIfExists($this->preProcessors, $preProcessor);

        return $this;
    }

    /**
     * @param PostProcessor $postProcessor
     * @return Presenter
     */
    public function addPostProcessor(PostProcessor $postProcessor) {
        $this->addIfNotExists($this->postProcessors, $postProcessor);

        return $this;
    }

    /**
     * @param MetaData $metaData
     * @return Presenter
     */
    public function addMetaData(MetaData $metaData) {
        $this->metaDateItems[] = $metaData;

        return $this;
    }

    /**
     * @return array
     * @throws SourceCouldNotBeEmptyException
     */
    public function present() : array {
        $source = $this->source();
        if (empty($source))
            throw new SourceCouldNotBeEmptyException();

        if (app(Presentation::class)->isListRegistered(get_class($this)))
            $this->loadFromRepository();

        foreach ($this->preProcessors as $preProcessor) {
            $source = $preProcessor->preProcess($source);
        }

        $collection = $this->execute($source);

        $processedCollection = new Collection();
        /** @var Model $item */
        foreach ($collection as $item) {
            $item = $item->toArray();
            foreach ($this->postProcessors as $postProcessor) {
                $item = $postProcessor->postProcess($item);
            }
            $processedCollection->add($item);
        }

        return [
            'data' => $processedCollection->toArray(),
            'meta' => $this->getMetaData(),
        ];
    }

    /**
     * @return array
     */
    private function getMetaData() {
        $metaData = [];

        foreach (array_merge($this->metaDateItems, $this->preProcessors, $this->postProcessors) as $metaDataItem) {
            /** @var BaseUnit $metaDataItem */
            $data = $metaDataItem->getMetaData();
            if ($data != null && !empty($data)) {
                $metaData[$metaDataItem->getKey()] = $data;
            }
        }

        return $metaData;
    }

    /**
     * @param array $configuration
     */
    public function loadFromConfiguration(array $configuration) {
        foreach ($configuration as $key=>$value) {
            $processor = Processor::create($key, $value);

            if (!empty($processor)) {
                if ($processor instanceof PreProcessor) {
                    $this->addPreProcessor($processor);
                } else if ($processor instanceof PostProcessor) {
                    $this->addPostProcessor($processor);
                }
            } else {
                $this->addMetaData(new MetaData($key, $value));
            }
        }
    }

    /**
     *
     */
    public function loadFromRepository() {
        if (app(Presentation::class)->isListRegistered(get_class($this))) {
            $configuration = app(Presentation::class)->getListConfiguration(get_class($this));
            $this->loadFromConfiguration($configuration);
        }
    }

}


