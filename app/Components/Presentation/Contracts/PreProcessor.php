<?php


namespace App\Components\Presentation\Contracts;


abstract class PreProcessor extends Processor
{
    public abstract function preProcess($source);
}
