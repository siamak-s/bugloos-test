<?php

namespace App\Components\Presentation\Providers;

use App\Components\Presentation\Presentation;
use App\Components\Presentation\Presenters\ModelBasedEloquentBuilderPresenter;
use App\Lists\PersistedUserList;
use App\Lists\UserList;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\ServiceProvider;

class PresentationServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $presentation = new Presentation();

        $presentation->registerLists(
            PersistedUserList::class
        );

        $this->app->singleton(Presentation::class, function ($app) use ($presentation) {
            return $presentation;
        });

        Builder::macro('present', function () {
            return (new ModelBasedEloquentBuilderPresenter($this))->present();
        });
    }
}
