<?php


namespace App\Components\Presentation\PostProcessors;


use App\Components\Presentation\Contracts\PostProcessor;
use App\Components\Presentation\Utils\FieldCollection;

class FieldFormatPostProcessor extends PostProcessor
{
    protected FieldCollection $collection;

    /**
     * FieldFormatPostProcessor constructor.
     * @param FieldCollection|null $collection
     */
    public function __construct(FieldCollection $collection = null)
    {
        $this->collection = $collection ?? new FieldCollection();
    }

    public function postProcess(array $values): array
    {
        // TODO do some formatting stuffs
        return $values;
    }
}
