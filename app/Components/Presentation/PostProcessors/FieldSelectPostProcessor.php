<?php


namespace App\Components\Presentation\PostProcessors;


use App\Components\Presentation\Contracts\PostProcessor;
use App\Components\Presentation\Utils\FieldCollection;

class FieldSelectPostProcessor extends PostProcessor
{
    const MISSING_VALUE_REPLACEMENT = 'NA';

    protected FieldCollection $collection;

    /**
     * FieldSelectPostProcessor constructor.
     * @param FieldCollection|null $collection
     */
    public function __construct(FieldCollection $collection = null)
    {
        $this->collection = $collection ?? new FieldCollection();
    }

    public function postProcess(array $values): array
    {
        $values = array_map(function ($fieldName ) use ($values) {
            return isset($values[$fieldName]) ? $fieldName : self::MISSING_VALUE_REPLACEMENT;
        }, $this->collection->getFieldNames());

        return $values;
    }
}
