<?php


namespace App\Components\Presentation\Traits;


use Illuminate\Support\Str;


trait TypesHelperTrait
{

    public static function getFamilyName()
    {
        return (new \ReflectionClass(static::class))->getShortName();
    }

    public static function getFamilyNamespace()
    {
        return property_exists(__class__, 'familyNamespace')
            ? static::familyNamespace
            : (new \ReflectionClass(static::class))->getNamespaceName();
    }

    public static function getPoolNamespace()
    {
        $poolInSeparateDirectory = false;

        $familyName = self::getFamilyName();
        $familyNamespace = self::getFamilyNamespace();

        if (property_exists(__class__, 'poolNamespace')) {
            return static::poolNamespace;
        } else {
            return $poolInSeparateDirectory
                ? $familyNamespace  . "\\" . Str::plural($familyName)
                : substr($familyNamespace, 0, strrpos($familyNamespace, "\\")) . "\\" . Str::plural($familyName);
        }
    }

    public static function getAvailableClassNames()
    {
        $familyName = self::getFamilyName();
        $poolNamespace = self::getPoolNamespace();

        $path = str_replace("\\", "/", substr(strchr($poolNamespace, "\\"), 1) . "\\*" . $familyName . ".php");

        $types = [];
        foreach (glob(app_path($path)) as $file) {
            $types[] = $poolNamespace . '\\' . basename($file, '.php');
        }

        return $types;
    }

    public static function getAvailableNames($plural = false)
    {
        $output = [];
        foreach (self::getAvailableClassNames() as $className) {
            $output[] = self::convertClassNameToName($className, $plural);
        }
        return $output;
    }

    public static function getInstanceOf($nameOrClassName)
    {
        return class_exists($nameOrClassName)
            ? self::getInstanceOfClassName($nameOrClassName)
            : self::getInstanceOfName($nameOrClassName);
    }

    public static function getInstanceOfClassName($className)
    {
        return new $className();
    }

    public static function getInstanceOfName($name)
    {
        return self::getInstanceOfClassName(self::convertNameToClassName($name));
    }

    public static function isValid($nameOrClassName)
    {
        return class_exists($nameOrClassName)
            ? self::isValidByClassName($nameOrClassName)
            : self::isValidByName($nameOrClassName);
    }

    public static function isValidByClassName($className)
    {
        return in_array($className, self::getAvailableClassNames());
    }

    public static function isValidByName($name)
    {
        return self::isValidByClassName(static::convertNameToClassName($name));
    }

    public static function convertNameToClassName($name)
    {
        return static::getPoolNamespace() . '\\' . Str::singular(Str::studly($name)) . self::getFamilyName();
    }

    public static function convertClassNameToName($className, $plural = false)
    {
        $output = Str::afterLast($className, "\\");
        $output = Str::replaceLast(self::getFamilyName(), '', $output);
        $output = Str::kebab($output);
//        $output = $plural ? Str::plural($output) : Str::singular($output);
        $output = $plural ? Str::plural($output) : $output;
        $output = Str::lower($output);

        return $output;
    }

    public static function getNameValidationString()
    {
        return implode(',', self::getAvailableNames());
    }
}
