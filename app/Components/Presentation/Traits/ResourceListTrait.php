<?php


namespace App\Components\Presentation\Traits;


use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Str;

trait ResourceListTrait
{
    protected $entity = null;

    protected function getEntity()
    {
        $entity = $this->entity ?? "App\\Models\\" . Str::replaceLast('Controller', '', Str::afterLast(get_class(), "\\"));
        if (!class_exists($entity))
            throw new ModelNotFoundException();

        return $entity;
    }

    public function index()
    {
        return response()->json($this->getEntity()::query()->present());
    }
}
