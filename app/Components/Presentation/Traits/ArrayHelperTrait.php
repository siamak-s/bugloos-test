<?php


namespace App\Components\Presentation\Traits;


use App\Components\Presentation\Exceptions\ItemAlreadyExistException;

trait ArrayHelperTrait
{
    private function add(&$targetArray, $item) {
        if ($this->searchForObjectOfType($targetArray, get_class($item)) >= 0) {
            throw new ItemAlreadyExistException();
        }

        $targetArray[] = $item;
    }

    private function addIfNotExists(&$targetArray, $item)  {
        if ($this->searchForObjectOfType($targetArray, get_class($item)) < 0) {
            $targetArray[] = $item;
        }
    }

    private function addOrReplaceIfExists(&$targetArray, $item) {
        $index = $this->searchForObjectOfType($targetArray, get_class($item));
        if ($index >= 0) {
            $targetArray[$index] = $item;
        } else {
            $targetArray[] = $item;
        }
    }

    private function remove(&$targetArray, $item) {
        $index = $this->searchForObjectOfType($targetArray, get_class($item));
        if ($index >= 0) {
            unset($targetArray[$index]);
        }
    }

    private function clear(&$targetArray) {
        $targetArray = [];
    }

    private function searchForObjectOfType($targetArray, $type) {
        $i = 0;

        foreach ($targetArray as $item) {
            if (get_class($item) == $type) {
                return $i;
            }

            $i++;
        }

        return -1;
    }
}
