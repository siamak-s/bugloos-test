<?php


namespace App\Components\Presentation\PreProcessors;


use App\Components\Presentation\Contracts\PreProcessor;
use App\Components\Presentation\Utils\FieldCollection;
use Spatie\QueryBuilder\QueryBuilder;

class EloquentSortPreProcessor extends PreProcessor
{
    protected FieldCollection $collection;

    /**
     * EloquentSortPreProcessor constructor.
     * @param FieldCollection|null $collection
     */
    public function __construct(FieldCollection $collection = null)
    {
        $this->collection = $collection ?? new FieldCollection();
    }

    /**
     * @param QueryBuilder $source
     * @return QueryBuilder
     */
    public function preProcess($source)
    {
        return $source->allowedSorts($this->collection->getFieldNames());
    }

    /**
     * @return array
     */
    public function getMetaData(): array
    {
        return $this->collection->getFieldNames();
    }
}
