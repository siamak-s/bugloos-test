<?php


namespace App\Components\Presentation\PreProcessors;


use App\Components\Presentation\Contracts\PreProcessor;
use App\Components\Presentation\Utils\FieldCollection;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class EloquentFilterPreProcessor extends PreProcessor
{
    protected FieldCollection $collection;

    /**
     * EloquentFilterPreProcessor constructor.
     * @param FieldCollection|null $collection
     */
    public function __construct(FieldCollection $collection = null)
    {
        $this->collection = $collection ?? new FieldCollection();
    }

    /**
     * @param QueryBuilder $source
     * @return QueryBuilder
     */
    public function preProcess($source)
    {
        $filters = [];

        foreach ($this->collection->getFields() as $key=>$value) {
            if ($value == 'partial') {
                $filters[] = AllowedFilter::partial($key);
            } elseif ($value == 'exact') {
                $filters[] = AllowedFilter::exact($key);
            } elseif ($value == 'scope') {
                $filters[] = AllowedFilter::scope($key);
            }
        }

        return $source->allowedFilters($filters);
    }

    public function getMetaData(): array
    {
        return $this->collection->getFieldNames();
    }
}
