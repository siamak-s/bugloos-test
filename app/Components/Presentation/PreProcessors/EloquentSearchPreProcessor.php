<?php


namespace App\Components\Presentation\PreProcessors;


use App\Components\Presentation\Contracts\PreProcessor;
use App\Components\Presentation\Utils\FieldCollection;

class EloquentSearchPreProcessor extends PreProcessor
{
    const SEARCH_TOKEN = 'search';

    protected FieldCollection $collection;

    /**
     * EloquentSearchPreProcessor constructor.
     * @param FieldCollection|null $collection
     */
    public function __construct(FieldCollection $collection = null)
    {
        $this->collection = $collection ?? new FieldCollection();
    }

    /**
     * @param $source
     * @return mixed
     */
    public function preProcess($source)
    {
        $searchTerm = request()->input(self::SEARCH_TOKEN);

        if (!empty($searchTerm)) {
            $source->where(function ($innerQuery) use ($searchTerm) {
                foreach ($this->collection->getFields() as $key => $value) {
                    if ($value == 'partial') {
                        $innerQuery->Orwhere($key, 'LIKE', "%$searchTerm%");
                    } elseif ($value == 'exact') {
                        $innerQuery->Orwhere($key, '=', $searchTerm);
                    }
                }
            });
        }

        return $source;
    }

    /**
     * @return array
     */
    public function getMetaData(): array
    {
        return $this->collection->getFieldNames();
    }
}
