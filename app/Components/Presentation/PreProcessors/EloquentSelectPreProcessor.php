<?php


namespace App\Components\Presentation\PreProcessors;


use App\Components\Presentation\Contracts\PreProcessor;
use App\Components\Presentation\Utils\FieldCollection;
use Spatie\QueryBuilder\QueryBuilder;

class EloquentSelectPreProcessor extends PreProcessor
{

    protected FieldCollection $collection;

    /**
     * EloquentSelectPreProcessor constructor.
     * @param FieldCollection|null $collection
     */
    public function __construct(FieldCollection $collection = null)
    {
        $this->collection = $collection ?? new FieldCollection();
    }

    /**
     * @param QueryBuilder $source
     * @return QueryBuilder
     */
    public function preProcess($source)
    {
        return count($this->collection->getFieldNames()) > 0
            ? $source->select($this->collection->getFieldNames())
            : $source;
    }

    /**
     * @return array
     */
    public function getMetaData(): array
    {
        return $this->collection->getFieldNames();
    }
}
