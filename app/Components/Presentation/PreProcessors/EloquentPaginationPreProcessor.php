<?php


namespace App\Components\Presentation\PreProcessors;


use App\Components\Presentation\Contracts\PreProcessor;
use Illuminate\Pagination\Paginator;
use Spatie\QueryBuilder\QueryBuilder;

class EloquentPaginationPreProcessor extends PreProcessor
{
    const PAGE_PARAMETER_NAME = 'page';
    const PER_PAGE_PARAMETER_NAME = 'per_page';

    private array $metaData = [];

    protected $perPage;

    /**
     * EloquentPaginationPreProcessor constructor.
     * @param $perPage
     */
    public function __construct($perPage = 10)
    {
        $this->perPage = $perPage;
    }

    /**
     * @param QueryBuilder $source
     * @return mixed
     */
    public function preProcess($source)
    {
        $perPage = request()->input(self::PER_PAGE_PARAMETER_NAME);
        $perPage = is_numeric($perPage) ? (int) $perPage : $this->perPage;

        $page = Paginator::resolveCurrentPage(self::PAGE_PARAMETER_NAME);

        $total = $source->toBase()->getCountForPagination();

        $this->metaData = [
            'total' => $total,
            'per_page' => $perPage,
            'page' => $page,
            'total_pages' => ceil($total / $perPage),
        ];

        return $source->forPage($page, $perPage);
    }

    public function getMetaData(): array
    {
        return $this->metaData;
    }
}
