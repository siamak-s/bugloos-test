<?php


namespace App\Components\Presentation\Exceptions;


use Exception;

class SourceCouldNotBeEmptyException extends Exception
{
    protected $message = 'Source could not be empty';
}
