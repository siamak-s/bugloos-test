<?php


namespace App\Components\Presentation\Exceptions;


use Exception;

class ItemAlreadyExistException extends Exception
{
    protected $message = 'Item already exists.';
}
