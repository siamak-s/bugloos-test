<?php


namespace App\Components\Presentation\Utils;


use App\Components\Presentation\Contracts\BaseUnit;

class MetaData implements BaseUnit
{
    private string $key;
    private array $data;

    /**
     * MetaData constructor.
     * @param string $key
     * @param array $data
     */
    public function __construct(string $key, array $data)
    {
        $this->key = $key;
        $this->data = $data;
    }

    public function getKey(): string
    {
        return $this->key;
    }

    public function getMetaData(): array
    {
        return $this->data;
    }
}
