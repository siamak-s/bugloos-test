<?php


namespace App\Components\Presentation\Utils;


class FieldCollection
{
    private array $fields = [];

    /**
     * @param array $items
     * @return FieldCollection
     */
    public static function fromArray(array $items = []) {
        return new self(self::isAssociative($items) ? $items : array_flip($items));
    }

    /**
     * FieldCollection constructor.
     * @param array $fields
     */
    public function __construct($fields = [])
    {
        $this->fields = $fields;
    }

    /**
     * @return array
     */
    public function getFields(): array
    {
        return $this->fields;
    }

    /**
     * @return array
     */
    public function getFieldNames() : array
    {
        return array_keys($this->fields);
    }

    /**
     * @return array
     */
    public function getAttributeValues() : array
    {
        return array_values($this->fields);
    }

    /**
     * @param string $field
     * @param $value
     */
    public function addField(string $field, $value = 0) {
        if (!in_array($field, array_keys($this->fields))) {
            $this->fields[$field] = $value;
        }
    }

    /**
     * @param string $field
     */
    public function removeField(string $field) {
        if (($key = array_search($field, array_keys($this->fields))) !== FALSE) {
            unset($this->fields[$key]);
        }
    }

    /**
     * @param string $field
     * @param $value
     */
    public function updateField(string $field, $value) {
        if (in_array($field, array_keys($this->fields))) {
            $this->fields[$field] = $value;
        }
    }

    /**
     * @param string $field
     * @param $value
     */
    public function addOrUpdateField(string $field, $value = 0) {
        $this->fields[$field] = $value;
    }

    /**
     *
     */
    public function clear() {
        $this->fields = [];
    }

    /**
     * @param array $items
     */
    public function mergeWithArray(array $items) {
        if (self::isAssociative($items)) {
            foreach ($items as $key=>$value) {
                $this->addOrUpdateField($key, $value);
            }
        }
    }

    /**
     * @param FieldCollection $collection
     */
    public function mergeWithKeyValueFieldCollection(FieldCollection $collection) {
        $this->mergeWithArray($collection->getFields());
    }

    /**
     * @param array $arr
     * @return bool
     */
    public static function isAssociative(array $arr) : bool
    {
        if (array() === $arr)
            return false;

        return array_keys($arr) !== range(0, count($arr) - 1);
    }
}
