<?php


namespace App\Components\Presentation;


use App\Models\ListConfiguration;
use Illuminate\Support\Facades\Cache;

class Presentation
{
    const CACHE_KEY = 'presentation_lists_configuration';

    private array $registeredLists = [];

    private array $listConfigurations = [];

    public function registerLists(string ...$names) {
        $names = is_array($names) ? $names : [$names];
        foreach ($names as $name) {
            if (class_exists($name) && !in_array($name, $this->registeredLists)) {
                $this->registeredLists[] = $name;
            }
        }
    }

    public function isListRegistered(string $name) {
        return in_array($name, $this->registeredLists);
    }

    public function getListConfiguration(string $name) {
//        $this->forgetCache();
        $this->loadFromCache();

        $configuration = [];
        foreach ($this->listConfigurations as $listConfiguration) {
            if ($listConfiguration['list_name'] == $name) {
                $configuration[$listConfiguration['unit_name']] = $listConfiguration['data'];
            }
        }
        return $configuration;
    }

    public function loadFromCache() {
        $this->listConfigurations = Cache::rememberForever(self::CACHE_KEY, function () {
            return $this->loadFromDatabase();
        });
    }

    public function loadFromDatabase() {
        return ListConfiguration::query()->get()->toArray();
    }

    public function forgetCache() {
        Cache::forget(self::CACHE_KEY);
    }
}
