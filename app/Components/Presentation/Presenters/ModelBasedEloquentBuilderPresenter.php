<?php


namespace App\Components\Presentation\Presenters;


use App\Components\Presentation\Exceptions\SourceCouldNotBeEmptyException;
use App\Components\Presentation\PostProcessors\FieldFormatPostProcessor;
use App\Components\Presentation\PreProcessors\EloquentFilterPreProcessor;
use App\Components\Presentation\PreProcessors\EloquentPaginationPreProcessor;
use App\Components\Presentation\PreProcessors\EloquentSearchPreProcessor;
use App\Components\Presentation\PreProcessors\EloquentSelectPreProcessor;
use App\Components\Presentation\PreProcessors\EloquentSortPreProcessor;
use App\Components\Presentation\Utils\FieldCollection;
use App\Components\Presentation\Utils\MetaData;
use App\Models\Model;

class ModelBasedEloquentBuilderPresenter extends EloquentBuilderPresenter
{

    /**
     * @return array
     * @throws SourceCouldNotBeEmptyException
     */
    public function present(): array
    {
        /** @var Model $model */
        $model = $this->source()->getModel();

        $this->addPreProcessor(new EloquentSortPreProcessor(FieldCollection::fromArray($model->getSortables())));
        $this->addPreProcessor(new EloquentFilterPreProcessor(FieldCollection::fromArray($model->getFilterables())));
        $this->addPreProcessor(new EloquentSearchPreProcessor(FieldCollection::fromArray($model->getSearchables())));
        $this->addPreProcessor(new EloquentSelectPreProcessor(FieldCollection::fromArray($model->getVisible())));
        $this->addPreProcessor(new EloquentPaginationPreProcessor());

//        $this->addPostProcessor(new FieldSelectPostProcessor(FieldCollection::fromArray($model->getVisible())));
        $this->addPostProcessor(new FieldFormatPostProcessor(FieldCollection::fromArray()));

        $this->addMetaData(new MetaData('labels', $model->getAttributes()));
        $this->addMetaData(new MetaData('widths', $model->getColumnWidths()));

        return parent::present();
    }

}
