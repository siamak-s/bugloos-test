<?php


namespace App\Components\Presentation\Presenters;


use App\Components\Presentation\Contracts\Presenter;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Collection;
use Spatie\QueryBuilder\QueryBuilder;

class EloquentBuilderPresenter extends Presenter
{
    const CACHE_TIME = 180; // seconds

    /**
     * @var QueryBuilder
     */
    private QueryBuilder $source;

    /**
     * EloquentPresenter constructor.
     * @param Builder $source
     * @param array $configuration
     */
    public function __construct(Builder $source, array $configuration = [])
    {
        $this->source = QueryBuilder::for($source);

        parent::__construct($configuration);
    }

    /**
     * @param $source
     * @return Collection
     */
    protected function execute($source): Collection
    {
        return $source->cacheFor(now()->addSeconds(self::CACHE_TIME))->get();
    }

    /**
     * @return QueryBuilder
     */
    protected function source()
    {
        return $this->source;
    }
}
