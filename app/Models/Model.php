<?php


namespace App\Models;


use Rennokki\QueryCache\Traits\QueryCacheable;

class Model extends \Illuminate\Database\Eloquent\Model
{
    use QueryCacheable;

    protected $sortables = [];

    protected $filterables = [];

    protected $columnWidths = [];

    protected $labels = [];

    public function getColumnWidths() {
        return $this->columnWidths;
    }

    public function getLabels() {
        return $this->labels;
    }

    public function getSortables() {
        return $this->sortables;
    }

    public function getFilterables() {
        return $this->filterables;
    }

    public function getSearchables() {
        $searchable = [];

        foreach ($this->getFilterables() as $key=>$value) {
            if (substr($key, 0, 3) != 'is_' && substr($key, 0, 4) != 'has_') {
                $searchable[$key] = $value;
            }
        }

        return $searchable;
    }
}
