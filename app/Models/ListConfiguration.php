<?php


namespace App\Models;


class ListConfiguration extends Model
{
    protected $fillable = [
        'list_name',
        'unit_name',
        'data',
    ];

    protected $casts = [
        'data' => 'array'
    ];
}
