<?php


namespace App\Http\Controllers\API\V1;


use App\Components\Presentation\Presentation;
use App\Http\Controllers\Controller;
use App\Models\ListConfiguration;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class ListConfigurationController extends Controller
{
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'list_name' => ['required', function ($attribute, $value, $fail) {
                if (!app(Presentation::class)->isListRegistered('App\\Lists\\' . Str::studly($value))) {
                    $fail('List configuration is not modifiable.');
                }
            }],
            'unit_name' => ['required'],
            'data' => ['required', 'array'],
        ]);

        $validatedData['list_name'] = 'App\\Lists\\' . Str::studly($validatedData['list_name']);

        $listConfiguration = ListConfiguration::firstOrNew([
            'list_name' => $validatedData['list_name'],
            'unit_name' => $validatedData['unit_name']
        ]);

        $listConfiguration->data = $validatedData['data'];
        $listConfiguration->save();

        app(Presentation::class)->forgetCache();

        return response()->json(['success' => true]);
    }
}
