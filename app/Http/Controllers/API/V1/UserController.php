<?php


namespace App\Http\Controllers\API\V1;


use App\Components\Presentation\PostProcessors\FieldFormatPostProcessor;
use App\Components\Presentation\PreProcessors\EloquentFilterPreProcessor;
use App\Components\Presentation\PreProcessors\EloquentPaginationPreProcessor;
use App\Components\Presentation\PreProcessors\EloquentSearchPreProcessor;
use App\Components\Presentation\PreProcessors\EloquentSelectPreProcessor;
use App\Components\Presentation\PreProcessors\EloquentSortPreProcessor;
use App\Components\Presentation\Presenters\EloquentBuilderPresenter;
use App\Components\Presentation\Traits\ResourceListTrait;
use App\Components\Presentation\Utils\FieldCollection;
use App\Components\Presentation\Utils\MetaData;
use App\Http\Controllers\Controller;
use App\Lists\PersistedUserList;
use App\Lists\UserList;
use App\Models\User;

class UserController extends Controller
{
    use ResourceListTrait;

    public function index2()
    {
        $source = User::query();

        $presenter = new EloquentBuilderPresenter($source);

        $presenter = $presenter
            ->addPreProcessor(new EloquentSortPreProcessor(FieldCollection::fromArray([
             'id',
             'name',
        ])))
            ->addPreProcessor(new EloquentFilterPreProcessor(FieldCollection::fromArray([
            'name',
            'email',
        ])))
            ->addPreProcessor(new EloquentSearchPreProcessor(FieldCollection::fromArray([
            'name',
            'email',
        ])))
            ->addPreProcessor(new EloquentSelectPreProcessor(FieldCollection::fromArray([
            'id',
            'name',
            'email',
//            'email_verified_at',
//            'created_at',
//            'updated_at',
        ])))
            ->addPreProcessor(new EloquentPaginationPreProcessor(3))
            ->addPostProcessor(new FieldFormatPostProcessor())
            ->addMetaData(new MetaData('labels', [
            'name' => 'Name',
            'email' => 'E-Mail',
        ]))
            ->addMetaData(new MetaData('widths', [
            'id' => 100,
            'name' => 200,
            'email' => 500,
        ]));

        return response()->json($presenter->present());
    }

    public function index3() {
        $source = User::query();

        $configuration = [
            'eloquent-sort' => [
                'id',
                'name',
            ],
            'eloquent-filter' => [
                'name' => 'partial',
                'email' => 'partial',
            ],
            'eloquent-search' => [
                'name' => 'partial',
                'email' => 'partial',
            ],
            'eloquent-select' => [
                'id',
                'name',
                'email',
            ],
            'eloquent-pagination' => [
                'perPage' => 2,
            ],
            'labels' => [
                'name' => 'Name',
                'email' => 'E-Mail',
            ],
            'widths' => [
                'id' => 100,
                'name' => 200,
                'email' => 500,
            ],
        ];

        $presenter = new EloquentBuilderPresenter($source, $configuration);

        return response()->json($presenter->present());
    }

    public function index4() {
        $list = new UserList();

        return response()->json($list->present());
    }

    public function index5() {
        return response()->json((new PersistedUserList())->present());
    }
}
