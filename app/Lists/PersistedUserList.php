<?php


namespace App\Lists;



use App\Components\Presentation\PostProcessors\FieldFormatPostProcessor;
use App\Components\Presentation\PreProcessors\EloquentFilterPreProcessor;
use App\Components\Presentation\PreProcessors\EloquentPaginationPreProcessor;
use App\Components\Presentation\PreProcessors\EloquentSearchPreProcessor;
use App\Components\Presentation\PreProcessors\EloquentSelectPreProcessor;
use App\Components\Presentation\PreProcessors\EloquentSortPreProcessor;
use App\Components\Presentation\Presenters\EloquentBuilderPresenter;
use App\Components\Presentation\Utils\FieldCollection;
use App\Components\Presentation\Utils\MetaData;
use App\Models\User;

class PersistedUserList extends EloquentBuilderPresenter
{
    public function __construct()
    {
        $source = User::query();

        $configuration = [
            'eloquent-sort' => [
                'id',
                'name',
            ],
            'eloquent-filter' => [
                'name' => 'partial',
                'email' => 'partial',
            ],
            'eloquent-search' => [
                'name' => 'partial',
                'email' => 'partial',
            ],
            'eloquent-select' => [
                'id',
                'name',
                'email',
            ],
            'eloquent-pagination' => [
                'perPage' => 10,
            ],
            'labels' => [
                'name' => 'Name',
                'email' => 'E-Mail',
            ],
            'widths' => [
                'id' => 100,
                'name' => 200,
                'email' => 500,
            ],
        ];

        parent::__construct($source, $configuration);
    }
}
