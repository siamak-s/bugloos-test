<?php


namespace App\Lists;



use App\Components\Presentation\PostProcessors\FieldFormatPostProcessor;
use App\Components\Presentation\PreProcessors\EloquentFilterPreProcessor;
use App\Components\Presentation\PreProcessors\EloquentPaginationPreProcessor;
use App\Components\Presentation\PreProcessors\EloquentSearchPreProcessor;
use App\Components\Presentation\PreProcessors\EloquentSelectPreProcessor;
use App\Components\Presentation\PreProcessors\EloquentSortPreProcessor;
use App\Components\Presentation\Presenters\EloquentBuilderPresenter;
use App\Components\Presentation\Utils\FieldCollection;
use App\Components\Presentation\Utils\MetaData;
use App\Models\User;

class UserList extends EloquentBuilderPresenter
{
    public function __construct()
    {
        $source = User::query();

        $this->addPreProcessor(new EloquentSortPreProcessor(FieldCollection::fromArray([
            'id',
            'name',
        ])));
        $this->addPreProcessor(new EloquentFilterPreProcessor(FieldCollection::fromArray([
            'name' => 'partial',
            'email' => 'partial',
        ])));
        $this->addPreProcessor(new EloquentSearchPreProcessor(FieldCollection::fromArray([
            'name' => 'partial',
            'email' => 'partial',
        ])));
        $this->addPreProcessor(new EloquentSelectPreProcessor(FieldCollection::fromArray([
            'id',
            'name',
            'email',
            'created_at',
        ])));
        $this->addPreProcessor(new EloquentPaginationPreProcessor(5));

        $this->addPostProcessor(new FieldFormatPostProcessor());

        $this->addMetaData(new MetaData('labels', [
            'name' => 'Name',
            'email' => 'E-Mail',
        ]));
        $this->addMetaData(new MetaData('widths', [
            'id' => 100,
            'name' => 200,
            'email' => 500,
        ]));

        parent::__construct($source);
    }
}
